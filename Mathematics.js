﻿$(document).ready(function () {
  LoadMathematics();
});

function Rearrange() {

  var Middle = document.getElementById('Mid');

  var viewPortHeight = $(window).height();

  var Margin = (viewPortHeight - 860) / 2;

  if (Margin < 0) { Margin = 0 }

  Middle.style.marginTop = Margin + 'px';

}

function LoadMathematics() {

  window.addEventListener('resize', function (event) {

    Rearrange();

  }, true);

  document.body.innerHTML = '';

  window.OnlyPositiveQuestions = true;
  window.OnlyPositiveAnswers = true;
  window.OnlyPlusMinus = true;
  window.MaxQuestionNumber = 10;

  var Half = document.createElement('div');
  Half.id = 'Half';
  document.body.appendChild(Half);

  var Mid = document.createElement('div');
  Mid.id = 'Mid';
  Half.appendChild(Mid);

  var Message = document.createElement('div');
  Message.id = 'Message';
  Half.appendChild(Message);
  window.Message = Message;

  var QuestionBlock = document.createElement('div');
  QuestionBlock.className = "Stretch";
  Mid.appendChild(QuestionBlock);

  var Q1 = new Block(QuestionBlock, GetRandomNumber(window.MaxQuestionNumber), GetRandomColor());
  var Q2 = new Block(QuestionBlock, GetRandomSign(), GetRandomColor());
  var Q3 = new Block(QuestionBlock, GetRandomNumber(window.MaxQuestionNumber), GetRandomColor());

  var EqualBlock = document.createElement('div');
  EqualBlock.className = "Stretch";
  Mid.appendChild(EqualBlock);

  new Block(EqualBlock, 'a', GetRandomColor());
  var Q4 = new Block(EqualBlock, '=', GetRandomColor());

  var Q1V = parseInt(Q1.Div.innerHTML);
  var Q2V = Q2.Div.innerHTML.toString();
  var Q3V = parseInt(Q3.Div.innerHTML);

  var Answer = 0;
  window.Answer = null;

  switch (Q2V) {
    case '+':
      Answer = Q1V + Q3V;
      break;
    case '-':
      Answer = Q1V - Q3V;
      break;
    case '×':
      Answer = Q1V * Q3V;
      break;
    case '÷':
      Answer = Q1V / Q3V;
      break;
  }

  var AnswerList = [];
  AnswerList.push(Answer);

  var AnsBlock = document.createElement('div');
  AnsBlock.className = "Stretch";
  Mid.appendChild(AnsBlock);

  var ran = parseInt(Math.random() * 3);

  var DivList = [];

  var A0 = document.createElement('div');

  if (ran == 0) {
    A0 = new AnswerBlock(AnsBlock, Answer, GetRandomColor());
    DivList.push(A0);
  }
  ran = ran - 1;

  var NextNumber = GetRandomNumber(window.MaxQuestionNumber * 2);
  while (ArrayContains(NextNumber, AnswerList)) {
    NextNumber = GetRandomNumber(window.MaxQuestionNumber * 2);
  }

  var A1 = new AnswerBlock(AnsBlock, NextNumber, GetRandomColor());
  DivList.push(A1);

  if (ran == 0) {
    A0 = new AnswerBlock(AnsBlock, Answer, GetRandomColor());
    DivList.push(A0);
  }
  ran = ran - 1;

  while (ArrayContains(NextNumber, AnswerList)) {
    NextNumber = GetRandomNumber(window.MaxQuestionNumber * 2);
  }

  var A2 = new AnswerBlock(AnsBlock, NextNumber, GetRandomColor());
  DivList.push(A2);

  if (ran == 0) {
    A0 = new AnswerBlock(AnsBlock, Answer, GetRandomColor());
    DivList.push(A0);
  }
  ran = ran - 1;

  if (Answer.toString().indexOf('.') != -1) {
    LoadMathematics();
    return;
  }

  if (window.OnlyPositiveAnswers && (parseInt(Answer) < 0)) {
    LoadMathematics();
    return;
  }

  window.Answer = Answer;

  Rearrange();

  DivList.push(Q4);
  DivList.push(Q3);
  DivList.push(Q2);
  DivList.push(Q1);

  var timer = setInterval(function () {

    if (DivList.length == 0) {
      clearInterval(timer);
    } else {

      var Temp = DivList.pop();
      Temp.Div.style.opacity = 1;
    }


  }, 100);

}

function GetRandomColor() {

  var ran = parseInt(Math.random() * 10);

  var result = '#c0392b'

  switch (ran) {
    case 0:
      result = '#1abc9c';
      break;
    case 1:
      result = '#2ecc71';
      break;
    case 2:
      result = '#3498db';
      break;
    case 3:
      result = '#9b59b6';
      break;
    case 4:
      result = '#34495e';
      break;
    case 5:
      result = '#e67e22';
      break;
    case 6:
      result = '#f39c12';
      break;
    case 7:
      result = '#e74c3c';
      break;
    case 8:
      result = '#95a5a6';
      break;
    case 9:
      result = '#27ae60';
      break;
  }

  return result;

}

function ArrayContains(val, AnswerList) {

  for (i = 0; i < AnswerList.length; i++) {
    if (AnswerList[i].toString() == val.toString()){
      return true;
    }
  }

  AnswerList.push(val);

  return false;

}

function GetRandomNumber(val) {

    if (window.OnlyPositiveQuestions) {
      return parseInt(Math.random() * val) + 1;
    } else {
      return parseInt(Math.random() * (val * 2)) + 1 - val;
    }

}

function GetRandomSign() {

  var result = '+'

  if (window.OnlyPlusMinus) {
    var ran = parseInt(Math.random() * 2);
  } else {
    var ran = parseInt(Math.random() * 4);
  }

  switch (ran) {
    case 0:
      result = '+';
      break;
    case 1:
      result = '-';
      break;
    case 2:
      result = '×';
      break;
    case 3:
      result = '÷';
      break;
  }

  return result;

}

function ShowMessage(text) {

  window.Message.innerHTML = text;
  window.Message.style.opacity = 1;
  window.Message.style.display = 'block';

  var timer = setInterval(function () {

    window.Message.style.opacity = 0;
    window.Message.style.display = 'none';

    if (window.Message.innerHTML == 'CORRECT!') {
      clearInterval(timer);
      LoadMathematics();
      return;
    }

  }, 1000);

}

function Block(Parent, text, bgcolor) {

  this.Div = document.createElement('div');
  this.Div.innerHTML = text;
  this.Div.className = "Block";
  this.Div.style.backgroundColor = bgcolor;
  this.Parent = Parent;

  if (!this.Parent) {
    document.body.appendChild(this.Div);
  } else {
    this.Parent.appendChild(this.Div);
  }

  var Me = this;

}

function AnswerBlock(Parent, text, bgcolor) {

  this.Div = document.createElement('div');
  this.Div.innerHTML = text;
  this.Div.className = "Block";
  this.Div.style.backgroundColor = bgcolor;
  this.Parent = Parent;
  this.text = text;

  var InnerDiv = document.createElement('div');
  InnerDiv.innerHTML = '?';
  InnerDiv.className = "InnerBlock";
  this.Div.appendChild(InnerDiv);

  if (!this.Parent) {
    document.body.appendChild(this.Div);
  } else {
    this.Parent.appendChild(this.Div);
  }

  var Me = this;

  this.Div.addEventListener('click', function (event) {

    if (window.Answer.toString() == Me.text.toString()) {
      ShowMessage('CORRECT!');
    } else {
      ShowMessage('WRONG!');
    }

  }, true);

}

