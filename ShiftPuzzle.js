﻿$(document).ready(function () {

  window.addEventListener('resize', function (event) {

    Rearrange();

  }, true);

  document.body.innerHTML = '';

  var Half = document.createElement('div');
  Half.id = 'Half';
  document.body.appendChild(Half);

  var Mid = document.createElement('div');
  Mid.id = 'Mid';
  Half.appendChild(Mid);

  var Message = document.createElement('div');
  Message.id = 'Message';
  Half.appendChild(Message);
  window.Message = Message;

  var MainBlock = document.createElement('div');
  MainBlock.className = "MainBlock";
  Mid.appendChild(MainBlock);
  window.MainBlock = MainBlock;

  AddLevelChooser(Half);

  ShowLevellChooser();
});

function Rearrange() {

  var Middle = document.getElementById('Mid');

  var viewPortHeight = $(window).height();

  var Margin = (viewPortHeight - window.PuzzleSize) / 2;

  if (Margin < 0) { Margin = 0 }

  Middle.style.marginTop = Margin + 'px';

}

function LoadShiftPuzzle() {

  window.PuzzleSize = 800;

  if (window.PuzzleNumber) {
    window.PuzzleNumber = window.PuzzleNumber + 1;
    if (window.PuzzleNumber > 4) {
      window.PuzzleNumber = 0;
    }
  } else {
    window.PuzzleNumber = parseInt(Math.random() * 5);
  }

  window.MainBlock.innerHTML = '';

  switch (window.PuzzleNumber) {
    case 0:
      window.BlockImage = 'url("Cow800.png")';
      break;
    case 1:
      window.BlockImage = 'url("Earth800.png")';
      break;
    case 2:
      window.BlockImage = 'url("Monkey800.png")';
      break;
    case 3:
      window.BlockImage = 'url("Penguin2800.png")';
      break;
    case 4:
      window.BlockImage = 'url("Penguin800.png")';
      break;
  }



  window.BlockList = [];
  window.EmptyCol = window.BlockNumbers - 1;
  window.EmptyRow = window.BlockNumbers - 1;

  for (row = (window.BlockNumbers - 1) ; row >= 0; row--) {
    for (col = (window.BlockNumbers - 1) ; col >= 0; col--) {
      if (!(col == (window.BlockNumbers - 1) && row == (window.BlockNumbers - 1))) {
        window.BlockList.push(new Block(window.MainBlock, col, row));
      }
    }
  }

  Rearrange();

  ShowBlocks();

}

function ShowBlocks() {

  var TempList = window.BlockList.slice();

  var timer = setInterval(function () {

    if (TempList.length == 0) {
      clearInterval(timer);
      Shuffle();
    } else {

      var Temp = TempList.pop();
      Temp.Div.style.opacity = 1;
    }


  }, 100);

}

function SwapBlocks() {

  var timer = setInterval(function () {


    var TempList = window.BlockList.slice();

    while (TempList.length > 1) {
      var Ran = parseInt(Math.random() * TempList.length);
      var One = TempList[Ran];

      TempList.splice(Ran, 1);

      var Ran = parseInt(Math.random() * TempList.length);
      var Two = TempList[Ran];

      TempList.splice(Ran, 1);

      One.PCol = Two.Col;
      One.PRow = Two.Row;

      Two.PCol = One.Col;
      Two.PRow = One.Row;

    }

    for (i = 0; i < window.BlockList.length; i++) {
      window.BlockList[i].Reset();
    }

    window.PuzzleReady = true;
    clearInterval(timer);

  }, 1000);

}

function Shuffle() {

  var timer = setInterval(function () {

    ////////////////////////////////////////////
    var PrevCol = -10;
    var PrevRow = -10;

    for (ind = 0; ind < 10000; ind++) {

      var NextBlockList = [];

      for (i = 0; i < window.BlockList.length; i++) {
        var t = window.BlockList[i];
        if (
          (
          ((Math.abs(t.PCol - window.EmptyCol) + Math.abs(t.PRow - window.EmptyRow)) == 1)
          )
          &&
          (!((PrevCol == t.PCol) && (PrevRow == t.PRow)))
          )
        {

          PrevCol = t.PCol;
          PrevRow = t.PRow;

          NextBlockList.push(t);
        }
      }



      var ShiftDone = false;

      for (i = 0; i < NextBlockList.length; i++) {
        var B = NextBlockList[i];
        if ((B.PCol == B.Col) && (B.PRow == B.Row)) {
          ShiftDone = true;
          B.Shift();
        }
      }

      if (!ShiftDone) {
        var ran = parseInt(Math.random() * NextBlockList.length);
        NextBlockList[ran].Shift();
      }

    }

    ////////////////////////////////////////////

    for (i = 0; i < window.BlockList.length; i++) {
      window.BlockList[i].Reset();
    }

    window.PuzzleReady = true;
    clearInterval(timer);

  }, 1000);

}

function CheckFinished() {

  for (i = 0; i < window.BlockList.length; i++) {
    var Block = window.BlockList[i];
    if (Block.Col != Block.PCol || Block.Row != Block.PRow) {
      return;
    }
  }

  ShowLevellChooser();
}

function Block(Parent, Col, Row) {

  this.Div = document.createElement('div');
  this.Div.className = "Block";
  this.Parent = Parent;
  this.Col = Col;
  this.Row = Row;
  this.PCol = Col;
  this.PRow = Row;
  this.Done = false;
  this.Width = parseInt(window.PuzzleSize / window.BlockNumbers);

  this.Div.style.width = this.Width - 4 + 'px';
  this.Div.style.height = this.Width - 4 + 'px';
  this.Div.style.backgroundImage = window.BlockImage;

  if (!this.Parent) {
    document.body.appendChild(this.Div);
  } else {
    this.Parent.appendChild(this.Div);
  }

  var Me = this;

  this.Reset = function () {

    Me.Div.style.backgroundPosition = ' -' + parseInt(Me.Width * Me.Col).toString() + 'px -' + parseInt(Me.Width * Me.Row).toString() + 'px';
    Me.Div.style.marginLeft = parseInt(Me.Width * Me.PCol).toString() + 'px';
    Me.Div.style.marginTop = parseInt(Me.Width * Me.PRow).toString() + 'px';

    if ((Me.PCol == Me.Col) && (Me.PRow == Me.Row)) {
      Me.Div.style.border = 'solid 4px #D0000A';
    } else {
      Me.Div.style.border = 'solid 2px #90E6B2';
    }

  }

  Me.Reset();

  this.Div.addEventListener('click', function (event) {

    if (!window.PuzzleReady) {
      return;
    }

    Me.Shift();

    Me.Reset();

    CheckFinished();

  }, true);

  this.Shift = function () {

    if ((Math.abs(Me.PCol - window.EmptyCol) + Math.abs(Me.PRow - window.EmptyRow)) == 1) {

      var TCol = window.EmptyCol;
      var TRow = window.EmptyRow;

      window.EmptyCol = Me.PCol;
      window.EmptyRow = Me.PRow;

      Me.PCol = TCol;
      Me.PRow = TRow;

    }

  }

}

function ShowMessage(text) {

  window.Message.innerHTML = text;
  window.Message.style.opacity = 1;
  window.Message.style.display = 'block';

  var timer = setInterval(function () {

    window.Message.style.opacity = 0;
    window.Message.style.display = 'none';

    clearInterval(timer);
    LoadShiftPuzzle();
    return;


  }, 1000);

}

function AddLevelChooser(Parent) {

  window.BlockNumbers = 3;

  var LevelChooser = document.createElement('div');
  LevelChooser.id = 'LevelChooser';
  Parent.appendChild(LevelChooser);
  window.LevelChooser = LevelChooser;

  var Level1 = document.createElement('div');
  Level1.className = 'LevelButton';
  Level1.innerHTML = 'Level 1';
  LevelChooser.appendChild(Level1);

  var Level2 = document.createElement('div');
  Level2.className = 'LevelButton';
  Level2.innerHTML = 'Level 2';
  LevelChooser.appendChild(Level2);

  var Level3 = document.createElement('div');
  Level3.className = 'LevelButton';
  Level3.innerHTML = 'Level 3';
  LevelChooser.appendChild(Level3);

  var Level4 = document.createElement('div');
  Level4.className = 'LevelButton';
  Level4.innerHTML = 'Level 4';
  LevelChooser.appendChild(Level4);

  Level1.addEventListener('click', function (event) {

    window.LevelChooser.style.opacity = 0;
    window.LevelChooser.style.display = 'none';
    window.BlockNumbers = 3;
    LoadShiftPuzzle();

  }, true);

  Level2.addEventListener('click', function (event) {

    window.LevelChooser.style.opacity = 0;
    window.LevelChooser.style.display = 'none';
    window.BlockNumbers = 4;
    LoadShiftPuzzle();

  }, true);

  Level3.addEventListener('click', function (event) {

    window.LevelChooser.style.opacity = 0;
    window.LevelChooser.style.display = 'none';
    window.BlockNumbers = 5;
    LoadShiftPuzzle();

  }, true);

  Level4.addEventListener('click', function (event) {

    window.LevelChooser.style.opacity = 0;
    window.LevelChooser.style.display = 'none';
    window.BlockNumbers = 6;
    LoadShiftPuzzle();

  }, true);
}

function ShowLevellChooser() {

  window.LevelChooser.style.opacity = 1;
  window.LevelChooser.style.display = 'block';
  window.PuzzleReady = false;

}