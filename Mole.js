﻿$(document).ready(function () {
  LoadMole();
});

function Rearrange() {

  var Middle = document.getElementById('Mid');

  var viewPortHeight = $(window).height();

  var Margin = (viewPortHeight - 880) / 2;

  if (Margin < 0) { Margin = 0 }

  Middle.style.marginTop = Margin + 'px';

}

function LoadMole() {

  window.addEventListener('resize', function (event) {

    Rearrange();

  }, true);

  document.body.innerHTML = '';

  var Half = document.createElement('div');
  Half.id = 'Half';
  document.body.appendChild(Half);

  var Mid = document.createElement('div');
  Mid.id = 'Mid';
  Half.appendChild(Mid);

  var Message = document.createElement('div');
  Message.id = 'Message';
  Half.appendChild(Message);
  window.Message = Message;

  var MainBlock = document.createElement('div');
  MainBlock.className = "MainBlock";
  Mid.appendChild(MainBlock);

  window.MoleList = [];
  var Index = 0;

  for (col = 0; col < 4; col++) {
    for (row = 0; row < 4; row++) {
      window.MoleList.push(new Mole(MainBlock, col * 230, row * 180, Index));
      Index = Index + 1;
    }
  }

  ShowMessage("START");

  Rearrange();

}

function StartMoles() {

  window.MoleScore = 0;
  var TempList = window.MoleList.slice();

  var timer = setInterval(function () {

    if (TempList.length > 0) {

      var Ran = parseInt(Math.random() * TempList.length);
      var T = TempList[Ran];

      TempList.splice(Ran, 1);

      window.MoleList[T.Index].Start()
    } else {
      clearInterval(timer);
    }


  }, 1400);

  var timer2 = setInterval(function () {

    for (i = 0; i < window.MoleList.length; i++) {
      window.MoleList[i].Level = 0;
      window.MoleList[i].Div.style.display = 'none';
    }

    ShowMessage("SCORE: " + window.MoleScore);
    clearInterval(timer2);

  }, 60000);

}

function Mole(Parent, x, y, Index) {

  this.x = x;
  this.y = y;
  this.Index = Index;

  this.Div = document.createElement('div');
  this.Div.className = 'Mole';
  this.Div.style.marginLeft = this.x + 'px';
  this.Div.style.marginTop = this.y + 'px';
  this.Parent = Parent;

  this.Head = document.createElement('img');
  this.Head.className = 'Head';
  this.Head.src = 'head.png';
  this.Div.appendChild(this.Head);

  this.HeadHider = document.createElement('div');
  this.HeadHider.className = 'HeadHider';
  this.Div.appendChild(this.HeadHider);

  this.Dirt = document.createElement('img');
  this.Dirt.className = 'Dirt';
  this.Dirt.src = 'dirt.png';
  this.Div.appendChild(this.Dirt);

  this.Paws = document.createElement('img');
  this.Paws.className = 'Paws';
  this.Paws.src = 'paws.png';
  this.Div.appendChild(this.Paws);

  this.Parent.appendChild(this.Div);

  this.Level = 0;

  var Me = this;

  this.Start = function () {

    Me.StartTimer = setInterval(function () {

      Me.Dirt.style.opacity = '1';
      Me.Level = 1;

      Me.ShowPaws();

      clearInterval(Me.StartTimer);

    }, 2600);

  }

  this.ShowPaws = function () {

    Me.PawsTimer = setInterval(function () {

      Me.Head.style.opacity = '1';
      Me.Paws.style.opacity = '1';
      Me.Level = 2;

      Me.Lift();

      clearInterval(Me.PawsTimer);

    }, 400);

  }

  this.Lift = function () {

    Me.LiftTimer = setInterval(function () {

      Me.Level = 3;
      Me.Head.style.marginTop = '0px';

      clearInterval(Me.LiftTimer);

    }, 600);

  }

  this.Div.addEventListener('click', function (event) {

    clearInterval(Me.StartTimer);
    clearInterval(Me.PawsTimer);
    clearInterval(Me.LiftTimer);

    switch (Me.Level) {
      case -1:
        Me.Level = -1;
        Me.Dirt.style.opacity = '0';
        Me.Head.style.opacity = '0';
        Me.Start();
        break;
      case 1:
        Me.Level = -1;
        Me.Dirt.style.opacity = '0';
        Me.Head.style.opacity = '0';
        Me.Start();
        window.MoleScore = window.MoleScore + 1;
        break;
      case 2:
        Me.Level = 1;
        Me.Paws.style.opacity = '0';
        Me.ShowPaws();
        window.MoleScore = window.MoleScore + 1;
        break;
      case 3:
        Me.Level = 2;
        Me.Head.style.marginTop = '90px';
        Me.Lift();
        window.MoleScore = window.MoleScore + 1;
        break;
    }

  }, true);

}



function ShowMessage(text) {

  window.Message.innerHTML = text;
  window.Message.style.opacity = 1;
  window.Message.style.display = 'block';

}

window.Message.addEventListener('click', function (event) {

  if (window.Message.innerHTML == "START") {
    window.Message.style.opacity = 0;
    window.Message.style.display = 'none';
    StartMoles();
  } else {

    var timer = setInterval(function () {

      window.Message.innerHTML = "START"

    }, 1500);

    
  }

}, true);